import Ajv from "ajv";
import Config from "./utilities/config";
import { getDnsRecords, getLoggedInUser, getZones } from "./utilities/api";
import {
  type DnsRecordType,
  type FullZoneType,
  type zoneType,
} from "./utilities/types";
import { execSync } from "child_process";
import fs from "fs";
import path from "path";

function createDnsTxt(zone: zoneType, dnsRecords: DnsRecordType[]) {
  let txtContent = `;;\n;; Domain:     ${
    zone.name
  }.\n;; Exported:   ${new Date().toISOString()}\n;;\n`;
  for (const ns of zone.name_servers) {
    txtContent += `${zone.name}.\t86400\tIN\tNS\t${ns}.\n`;
  }
  txtContent += "\n;; A Records\n";
  for (const record of dnsRecords.filter((r: any) => r.type === "A")) {
    txtContent += `${record.name}.\t${record.ttl}\tIN\tA\t${record.content}\n`;
  }
  txtContent += "\n;; AAAA Records\n";
  for (const record of dnsRecords.filter((r: any) => r.type === "AAAA")) {
    txtContent += `${record.name}.\t${record.ttl}\tIN\tA\t${record.content}\n`;
  }
  txtContent += "\n;; CNAME Records\n";
  for (const record of dnsRecords.filter((r: any) => r.type === "CNAME")) {
    txtContent += `${record.name}.\t${record.ttl}\tIN\tCNAME\t${record.content}.\n`;
  }
  txtContent += "\n;; MX Records\n";
  for (const record of dnsRecords.filter((r: any) => r.type === "MX")) {
    txtContent += `${record.name}.\t${record.ttl}\tIN\tMX\t${record.priority} ${record.content}.\n`;
  }
  txtContent += "\n;; TXT Records\n";
  for (const record of dnsRecords.filter((r: any) => r.type === "TXT")) {
    txtContent += `${record.name}.\t${record.ttl}\tIN\tTXT\t"${record.content}"\n`;
  }
  return txtContent;
}

function stringDate(inDate: Date) {
  return `${inDate.getFullYear()}-${inDate.getMonth()}-${inDate.getDate()}`;
}

async function main() {
  const userInfo = await getLoggedInUser(
    Config.CLOUDFLARE_API_URL,
    Config.CLOUDFLARE_API_TOKEN,
  );
  const zones = await getZones(
    Config.CLOUDFLARE_API_URL,
    Config.CLOUDFLARE_API_TOKEN,
  );
  const tempDir = path.join(__dirname, "temp");
  if (fs.existsSync(tempDir)) {
    fs.rmdirSync(tempDir, { recursive: true });
  }
  if (!fs.existsSync(tempDir)) {
    fs.mkdirSync(tempDir);
  }
  let fullZones: FullZoneType[] = [];
  for (const zone of zones) {
    console.log(`Zone: ${zone.name}`);
    const records = await getDnsRecords(
      zone.id,
      Config.CLOUDFLARE_API_URL,
      Config.CLOUDFLARE_API_TOKEN,
    );
    const dnsTxt = createDnsTxt(zone, records);
    fs.writeFileSync(path.join(tempDir, `${zone.name}.txt`), dnsTxt);
    fullZones.push({ ...zone, dns_records: records });
  }
  const command = `zip -r output/${userInfo.email}_${stringDate(new Date())}.zip temp`;
  console.log(command);
  execSync(command);
  const outputJsonFilename = `output/${userInfo.email}_${stringDate(new Date())}.json`;
  fs.writeFileSync(outputJsonFilename, JSON.stringify(fullZones, null, 2));
  execSync(`gzip ${outputJsonFilename}`);
  if (fs.existsSync(tempDir)) {
    fs.rmdirSync(tempDir, { recursive: true });
  }
}

main()
  .then(() => {
    console.log("Done: ", new Date().toISOString());
    if (Config.WEBHOOK) {
      fetch(Config.WEBHOOK).then((response) => {
        console.log("Webhook response: ", response);
        process.exit(0);
      });
    } else {
      console.log("No webhook configured");
      process.exit(0);
    }
  })
  .catch((error) => {
    console.error("An error occurred:", error);
  });
