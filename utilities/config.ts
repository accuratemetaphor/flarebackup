import { Type, type Static } from "@sinclair/typebox";
import * as dotenv from "dotenv";
import Ajv from "ajv";

// Define the Config type
const CLOUDFLARE_API_TOKEN = "CLOUDFLARE_API_TOKEN";
const CLOUDFLARE_API_URL = "CLOUDFLARE_API_URL";
const WEBHOOK = "WEBHOOK";

const ConfigTypeBox = Type.Object({
  [CLOUDFLARE_API_TOKEN]: Type.String(),
  [CLOUDFLARE_API_URL]: Type.String(),
  [WEBHOOK]: Type.Optional(Type.String()),
});

type ConfigType = Static<typeof ConfigTypeBox>;

// Load environment variables from .env file
dotenv.config();

// Validate environment variables
const env: Partial<ConfigType> = {
  [CLOUDFLARE_API_TOKEN]: process.env[CLOUDFLARE_API_TOKEN],
  [CLOUDFLARE_API_URL]: process.env[CLOUDFLARE_API_URL],
  [WEBHOOK]: process.env[WEBHOOK],
};

const ajv = new Ajv();
const validate = ajv.compile(ConfigTypeBox);

if (!validate(env)) {
  throw new Error(
    `Invalid environment variables: ${ajv.errorsText(validate.errors)}`,
  );
}

// Export the environment variables with the correct types
const Config: ConfigType = env as ConfigType;

export default Config;
