import Ajv from "ajv";
import {
  ListDnsRecordsResponse,
  ListZonesResponse,
  UserInfoResponse,
  type ListDnsRecordsResponseType,
  type ListZonesResponseType,
  type UserInfoResponseType,
} from "./types";

const ajv = new Ajv();

const validateListDnsRecordsResponse = ajv.compile(ListDnsRecordsResponse);
const validateListZonesResponse = ajv.compile(ListZonesResponse);
const validateUserInfoResponse = ajv.compile(UserInfoResponse);

export async function getLoggedInUser(
  cloudflareUrl: string,
  cloudflareToken: string,
) {
  const userResponse = await fetch(`${cloudflareUrl}/user`, {
    headers: {
      Authorization: `Bearer ${cloudflareToken}`,
      "Content-Type": "application/json",
    },
  });
  const userDataJson = await userResponse.json();
  if (!validateUserInfoResponse(userDataJson)) {
    console.error(
      "Invalid response for User records:",
      userDataJson,
      validateUserInfoResponse.errors,
    );
    throw new Error("Invalid response for User records");
  }
  const userData = userDataJson as UserInfoResponseType;
  if (!userData.success) {
    console.error("Error fetching DNS records:", userData.errors);
    throw new Error("Error fetching DNS records");
  }
  return userData.result;
}

export async function getDnsRecords(
  zoneId: string,
  cloudflareUrl: string,
  cloudflareToken: string,
) {
  const dnsRecordsResponse = await fetch(
    `${cloudflareUrl}/zones/${zoneId}/dns_records`,
    {
      headers: {
        Authorization: `Bearer ${cloudflareToken}`,
        "Content-Type": "application/json",
      },
    },
  );
  const dnsRecordsDataJson = await dnsRecordsResponse.json();
  if (!validateListDnsRecordsResponse(dnsRecordsDataJson)) {
    console.error(
      "Invalid response for DNS records:",
      dnsRecordsDataJson,
      validateListDnsRecordsResponse.errors,
    );
    throw new Error("Invalid response for DNS records");
  }
  const dnsRecordsData = dnsRecordsDataJson as ListDnsRecordsResponseType;
  if (!dnsRecordsData.success) {
    console.error("Error fetching DNS records:", dnsRecordsData.errors);
    throw new Error("Error fetching DNS records");
  }
  return dnsRecordsData.result;
}

export async function getZones(cloudflareUrl: string, cloudflareToken: string) {
  const zonesResponse = await fetch(`${cloudflareUrl}/zones`, {
    headers: {
      Authorization: `Bearer ${cloudflareToken}`,
      "Content-Type": "application/json",
    },
  });
  const zonesDataJson = await zonesResponse.json();
  if (!validateListZonesResponse(zonesDataJson)) {
    console.error(
      "Invalid response for zones:",
      zonesDataJson,
      validateListZonesResponse.errors,
    );
    throw new Error("Invalid response for zones");
  }
  const zonesData = zonesDataJson as ListZonesResponseType;
  if (!zonesData.success) {
    console.error("Error fetching zones:", zonesData.errors);
    throw new Error("Error fetching zones");
  }
  return zonesData.result;
}
