import { Type, type Static } from "@sinclair/typebox";

const Owner = Type.Object({
  id: Type.Union([Type.String(), Type.Null()]),
  email: Type.Union([Type.String(), Type.Null()]),
  type: Type.Union([Type.String(), Type.Null()]),
});

const Account = Type.Object({
  id: Type.String(),
  name: Type.String(),
});

const Organization = Type.Object({
  id: Type.String(),
  name: Type.String(),
  status: Type.String(),
  permissions: Type.Array(Type.String()),
  roles: Type.Array(Type.String()),
});

const UserInfo = Type.Object({
  id: Type.String(),
  email: Type.String(),
  username: Type.String(),
  first_name: Type.Union([Type.String(), Type.Null()]),
  last_name: Type.Union([Type.String(), Type.Null()]),
  telephone: Type.Union([Type.String(), Type.Null()]),
  country: Type.Union([Type.String(), Type.Null()]),
  zipcode: Type.Union([Type.String(), Type.Null()]),
  two_factor_authentication_enabled: Type.Boolean(),
  two_factor_authentication_locked: Type.Boolean(),
  created_on: Type.String(),
  modified_on: Type.String(),
  organizations: Type.Array(Organization),
  has_pro_zones: Type.Boolean(),
  has_business_zones: Type.Boolean(),
  has_enterprise_zones: Type.Boolean(),
  suspended: Type.Boolean(),
  betas: Type.Array(Type.String()),
});

export const UserInfoResponse = Type.Object({
  result: UserInfo,
  success: Type.Boolean(),
  errors: Type.Array(Type.Any()),
  messages: Type.Array(Type.Any()),
});
export type UserInfoResponseType = Static<typeof UserInfoResponse>;

const Plan = Type.Object({
  id: Type.String(),
  name: Type.String(),
  price: Type.Number(),
  currency: Type.String(),
  frequency: Type.String(),
  legacy_id: Type.String(),
  is_subscribed: Type.Boolean(),
  can_subscribe: Type.Boolean(),
});

const DnsRecord = Type.Object({
  id: Type.String(),
  zone_id: Type.String(),
  zone_name: Type.String(),
  name: Type.String(),
  type: Type.String(),
  content: Type.String(),
  priority: Type.Optional(Type.Number()),
  proxiable: Type.Boolean(),
  proxied: Type.Boolean(),
  ttl: Type.Number(),
  locked: Type.Optional(Type.Boolean()),
  meta: Type.Object({
    auto_added: Type.Boolean(),
    managed_by_apps: Type.Boolean(),
    managed_by_argo_tunnel: Type.Boolean(),
  }),
  comment: Type.Union([Type.String(), Type.Null()]),
  tags: Type.Array(Type.String()),
  created_on: Type.String(),
  modified_on: Type.String(),
});
export type DnsRecordType = Static<typeof DnsRecord>;

const Zone = Type.Object({
  id: Type.String(),
  name: Type.String(),
  development_mode: Type.Number(),
  original_name_servers: Type.Union([Type.Array(Type.String()), Type.Null()]),
  original_registrar: Type.Union([Type.String(), Type.Null()]),
  original_dnshost: Type.Union([Type.String(), Type.Null()]),
  created_on: Type.String(),
  modified_on: Type.String(),
  activated_on: Type.String(),
  owner: Owner,
  account: Account,
  permissions: Type.Array(Type.String()),
  plan: Plan,
  plan_pending: Type.Optional(Plan),
  status: Type.String(),
  paused: Type.Boolean(),
  type: Type.String(),
  name_servers: Type.Array(Type.String()),
});
export type zoneType = Static<typeof Zone>;

export const FullZone = Type.Union([
  Zone,
  Type.Object({
    dns_records: Type.Array(DnsRecord),
  }),
]);
export type FullZoneType = Static<typeof FullZone>;

// Define the type for the response of listing DNS records
export const ListDnsRecordsResponse = Type.Object({
  success: Type.Boolean(),
  errors: Type.Array(Type.Any()),
  result: Type.Array(DnsRecord),
});

// Define the type for the response of listing zones
export const ListZonesResponse = Type.Object({
  success: Type.Boolean(),
  errors: Type.Array(Type.Any()),
  result: Type.Array(Zone),
});

export type ListDnsRecordsResponseType = Static<typeof ListDnsRecordsResponse>;
export type ListZonesResponseType = Static<typeof ListZonesResponse>;
