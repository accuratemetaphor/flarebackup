<img src="https://gitlab.com/accuratemetaphor/flarebackup/-/raw/main/assets/logo.svg" alt="Flarebackup logo" width="100" />

# Flarebackup

Flarebackup is a Typescript project that interacts with the [Cloudflare API](https://developers.cloudflare.com/fundamentals/api/) to fetch user and DNS information. It then writes this information to text files and compresses them into a zip file.

## Prerequisites

- Bun or Node.js
- Zip
- Gzip

## Installation

Clone the repository and navigate to the project directory. Install the dependencies with the following command:

```bash
bun install
```

## Configuration

The project uses environment variables for configuration. Copy the `example.env` file to `.env` and fill in your Cloudflare API token and URL:

```bash
cp example.env .env
```

The instructions for getting an API Token are at [https://developers.cloudflare.com/fundamentals/api/get-started/create-token/](https://developers.cloudflare.com/fundamentals/api/get-started/create-token/). The token needs read access to `User.User Details`, `Zone.Zone`, and `Zone.DNS`.

## Usage

To run the project, use the following command:

```bash
bun run index.ts
```

## Output

The output will be a zip file containing text files for each DNS zone, and a JSON file with user and DNS information. These files are located in the `output` directory.

## Disclaimer
This project is not affiliated, associated, authorized, endorsed by, or in any way officially connected with Cloudflare, Inc. or any of its subsidiaries or its affiliates. The official Cloudflare website can be found at https://www.cloudflare.com.

## License

[AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)
